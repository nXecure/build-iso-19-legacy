#!/bin/bash

THEME_DIR=$(dirname $(readlink -f $0))
source $THEME_DIR/../theme-functions.sh
start_theme "$@"

copy_file luckybackup.desktop 			/usr/share/applications/
copy_file luckybackup-gnome-su.desktop 	/usr/share/applications/
copy_file pybootchartgui.desktop 			/usr/share/applications/
copy_file xfburn.desktop 				/usr/share/applications/
copy_file ddm-nvidia.desktop 			/usr/share/applications/
copy_file display-im6.desktop			/usr/share/applications/ 
copy_file display-im6.q16.desktop			/usr/share/applications/
copy_file mcedit.desktop 				/usr/share/applications/
copy_file gftp.desktop 					/usr/share/applications/
copy_file gnome-ppp.desktop 			/usr/share/applications/
copy_file grsync.desktop 				/usr/share/applications/
copy_file guvcview.desktop 				/usr/share/applications/
copy_file hplj1020.desktop 				/usr/share/applications/
copy_file mtpaint.desktop 				/usr/share/applications/
copy_file ndisgtk.desktop 				/usr/share/applications/
copy_file pppoeconf.desktop 				/usr/share/applications/
copy_file python2.7.desktop 				/usr/share/applications/
copy_file python3.5.desktop 				/usr/share/applications/
copy_file qt5ct.desktop 					/usr/share/applications/
copy_file rxvt-unicode.desktop 			/usr/share/applications/
copy_file searchmonkey.desktop 			/usr/share/applications/
copy_file streamtuner2.desktop 			/usr/share/applications/
copy_file wpa_gui.desktop 				/usr/share/applications/
copy_file mpv.desktop 					/usr/share/applications/
copy_file yad-icon-browser.desktop 		/usr/share/applications/
copy_file htop.desktop 					/usr/share/applications/
copy_file mc.desktop 					/usr/share/applications/
copy_file umts-panel.desktop 			/usr/share/applications/
copy_file clipit.desktop 					/usr/share/applications/
copy_file arandr.desktop 				/usr/share/applications/
copy_file libreoffice-xsltfilter.desktop		/usr/share/applications/
copy_file mupdf.desktop					/usr/share/applications/
copy_file simple-scan.desktop			/usr/share/applications/
copy_file galternatives.desktop			/usr/share/applications/
copy_file lxde-audio-video.directory 		/usr/share/desktop-directories/
copy_file back.png 						/usr/share/wallpaper/grub/
copy_file msystem.png 					/usr/share/icons/
copy_file org.gnome.ArchiveManager.png 	/usr/share/icons/

copy_file grub 							/etc/default/
copy_file resolvconf 					/etc/default/
copy_file 10_linux 						/etc/grub.d/
copy_file 20_memtest86+ 				/etc/grub.d/
copy_file rc.local 						/etc/
copy_file bootchartd.conf 				/etc/
copy_file modules 						/etc/
copy_file 98vboxadd-xclient     			/etc/X11/Xsession.d/
copy_file sysctl.conf           				/etc/
copy_file hosts                 				/etc/
copy_file hosts.ORIGINAL        			/etc/
copy_file hosts.saved           				/etc/
copy_file mouse.conf 					/etc/skel/.desktop-session/ 
copy_file issue						/usr/share/antiX/
copy_file antixccslim.sh					/usr/local/bin/
copy_file environment 					/etc/
copy_file slim.conf 						/etc/
copy_file fbgrab 						/usr/bin/
copy_file config.py 						/usr/lib/python3/dist-packages/mps_youtube/
copy_file .bashrc 						/etc/skel/
copy_file user-dirs.defaults 				/etc/xdg 
copy_file lxde-applications.menu 			/etc/xdg/menus 
copy_file equivalents.html       			/usr/share/antiX/
copy_file ixquick-https.xml 				/usr/share/firefox-esr/distribution/searchplugins/common/
copy_file startpage-https.xml 			/usr/share/firefox-esr/distribution/searchplugins/common/
copy_file distribution.ini 				/usr/share/firefox-esr/distribution/
copy_dir dillo/                  				/etc/skel/.dillo/     --create
copy_dir icons/                  				/usr/share/antiX/icons/     --create
copy_dir connman/ 						/var/lib/connman/    --create 
copy_dir mps-youtube/ 					/etc/skel/.config/mps-youtube    --create

exit
